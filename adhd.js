chrome.extension.sendRequest(
	{method: "getLocalStorage", key: "adhd-enabled"},
	function(response) {
		if (response.data === "true")
			bindMouseMove();
	}
);

function bindMouseMove() {
	$('body').mousemove(function (e) {
		var $videos = $('video');
		$videos.each(function () {
			var $video = $(this);
			var offset = $video.offset();

			var x = e.pageX - offset.left;
			var y = e.pageY - offset.top;
			var w = $video.width();
			var h = $video.height();

			var speed;
			if (y < 0 || y > h || x < 0 || x > w) {
				speed = 1;
			} else {
				var ratio = 0.5 - y/h;
				if (ratio < 0)
					speed = 1 + ratio/0.5;
				else
					speed = 1 + ratio/0.5*5;
			}
			$video[0].playbackRate = speed;
		});
	});
}