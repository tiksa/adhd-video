$('document').ready(function () {
	var alreadyEnabled = localStorage["adhd-enabled"];
	if (alreadyEnabled === "true") {
		$('#adhd-enabled').prop('checked', true);
	}

	$('#adhd-enabled').change(function () {
		var enabled = $('#adhd-enabled').is(':checked');
		localStorage["adhd-enabled"] = enabled;
	});
});